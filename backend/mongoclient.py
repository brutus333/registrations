import os
from pymongo import MongoClient, errors
from pymongo.errors import ConnectionFailure

COLLECTION_NAME = 'registrations'

class MongoConnectionFailed(Exception):
    pass

class MongoRegistrations(object):
    def __init__(self):
        self.mongo_url = os.environ.get('MONGO_URL')
        self.client = self.get_mongo_client()
        self.db = self.client.registrations
        self.db.registrations.create_index("phone_number", unique=True)

    def find_all(self):
        return self.db.registrations.find()

    def create(self, registered_user):
        return self.db.registrations.insert_one(registered_user)

    def get_mongo_client(self):
        client = MongoClient(self.mongo_url, serverSelectionTimeoutMS=1000,
                             connectTimeoutMS=1000, retryReads=True)
        try:
            client.admin.command('ping')
        except ConnectionFailure:
            raise MongoConnectionFailed("Server not available")
        return client

    def health(self):
        try:
            self.client.server_info()
        except errors.ServerSelectionTimeoutError as err:
            raise MongoConnectionFailed(err)
