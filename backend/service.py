from mongoclient import MongoRegistrations
from schema import RegisteredUserSchema


class Service:
  def __init__(self, client=MongoRegistrations()):
    self.client = client

  def find_all_registrations(self):
    registrations  = self.client.find_all()
    return [self.dump(registration) for registration in registrations]

  def create_registration_for(self, registeredUser):
    self.client.create(registeredUser)
    return self.dump(registeredUser)

  def dump(self, data):
    return RegisteredUserSchema(exclude=['_id']).dump(data)

  def health(self):
    self.client.health()