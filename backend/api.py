from flask import Flask, json, g, request
from service import Service
from schema import RegisteredUserSchema
from flask_cors import CORS
from marshmallow import ValidationError
from pymongo.errors import DuplicateKeyError, ServerSelectionTimeoutError
from mongoclient import MongoConnectionFailed

app = Flask(__name__)

CORS(app)


@app.route("/registrations", methods=["GET"])
def index():
    try:
        all_registrations = Service().find_all_registrations()
    except ServerSelectionTimeoutError as e:
        return json_response({'error': f"Mongo DB is not responding: {e.details}"}, 503)
    return json_response(all_registrations)


@app.route("/registrations", methods=["POST"])
def create():
    if request.headers['Content-Type'] == 'application/json':
        app.logger.warning(request.json)
    else:
        return json_response({'error': 'Please send the request with application/json content type'}, 422)

    try:
        registered_user = RegisteredUserSchema().load(request.json)

    except ValidationError as e:
        return json_response({'error': e.normalized_messages()}, 422)
    try:
        service = Service().create_registration_for(registered_user)
    except DuplicateKeyError as e:
        return json_response({'error': e.details})
    except ServerSelectionTimeoutError as e:
        return json_response({'error': f"Mongo DB is not responding: {e.details}"}, 503)

    return json_response(service)

@app.route("/health", methods=["GET"])
def health():
    try:
        Service().health()
    except MongoConnectionFailed as e:
        return json_response({'error': f"Mongo DB is not responding: {e}"}, 503)
    return json_response("UP")

def json_response(payload, status=200):
    return (json.dumps(payload), status, {'content-type': 'application/json'})


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')