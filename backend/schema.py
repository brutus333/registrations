import phonenumbers
from marshmallow import Schema, fields, validates, ValidationError


def validate_phone_number(phone_number):
    try:
        phone = phonenumbers.parse(phone_number)
    except phonenumbers.NumberParseException as e:
        return False
    return phonenumbers.is_valid_number(phone) if phone else False

def validate_age(age):
    return age >= 18 and age < 130


class RegisteredUserSchema(Schema):
  _id = fields.Int(required=False)
  full_name = fields.Str(required=True)
  phone_number = fields.Str(required=True)
  age = fields.Integer(required=True)

  @validates("phone_number")
  def validate_phone_number(self,value):
      try:
          phone = phonenumbers.parse(value)
      except phonenumbers.NumberParseException as e:
          raise ValidationError(f"Cannot parse phone number format: {e}")
      if phone:
        if not phonenumbers.is_valid_number(phone):
            raise ValidationError("Phone number is not valid")

  @validates("age")
  def validate_age(self,value):
      if value < 18 or value >= 130:
          raise ValidationError("Age should be between 18 and 130")

