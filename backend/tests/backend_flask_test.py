import pytest
from api import app
from mongoclient import MongoRegistrations

@pytest.fixture
def client():
    with app.test_client() as client:
        yield client

@pytest.fixture
def cleanmongo():
    MongoRegistrations().client.drop_database('registrations')

@pytest.mark.usefixtures("cleanmongo")
def test_empty_registrations(client):
    rv = client.get('/registrations')
    assert b'[]' in rv.data

def test_add_valid_registrations(client):
    rv = client.post('/registrations',
                     json=dict(full_name='John Doe', age=45, phone_number='+33637032975'))
    assert rv.status == '200 OK'

def test_add_invalid_registrations(client):
    rv = client.post('/registrations',
                     json=dict(full_name='John Doe Junior', age=19, phone_number='+336370329711'))
    assert rv.status == '422 UNPROCESSABLE ENTITY'