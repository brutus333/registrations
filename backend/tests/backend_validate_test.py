import unittest
from schema import validate_phone_number, validate_age, RegisteredUserSchema
from marshmallow import ValidationError

class ValidationTestCase(unittest.TestCase):

    def test_registered_user(self):
        userSchema = RegisteredUserSchema().load({"age": 77, "full_name": "John Doe", "phone_number": "+33637032975"})
        self.assertIsNotNone(userSchema)
    def test_wrong_phone_number_format(self):
        with self.assertRaises(ValidationError):
            RegisteredUserSchema().load({"age": 77, "full_name": "John Doe", "phone_number": "+336370329751"})
    def test_wrong_age(self):
        with self.assertRaises(ValidationError):
            RegisteredUserSchema().load({"age": 6, "full_name": "John Doe", "phone_number": "+33637032975"})


if __name__ == '__main__':
    unittest.main()