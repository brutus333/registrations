# Registration app

## Requirements

- Allows registration to an event via a simple HTML form
- Shows a table with registered users and their details
- The following details need to be registered:
  * Full Name
  * Age
  * Phone Number

## Assumptions

- The app will not allow two registrations using same phone number
- Phone numbers should be validated for correctness
- Age should be between 18 and 130 (it's an event for grow ups)

## Architecture

The app is build with one persistency layer (mongodb), one backend written in python with Flask framework and one frontend written in react js.

## Deployment

Application can be deployed with docker-compose.

Ansible recipes for configuring a virtual machine with docker and docker-compose and that are starting the docker-compose stack are included.

Deployment stage is running the docker-compose in a Google, Container-Optimized OS. Due to the VM particularities docker-compose is run also in a container.
Ansible docker_container module was not possible to be used since COS has a limited python distribution.

## Testing

API is tested using gitlab pipeline. One example job: https://gitlab.com/brutus333/registrations/-/jobs/1428140128

## Update on 31-05-2023

## Deliverables

- source code for frontend & backend: https://gitlab.com/brutus333/registrations/
- Docker images: https://gitlab.com/brutus333/registrations/container_registry
- Application configuration files: https://gitlab.com/brutus333/registrations/-/blob/master/deployment/ansible/docker-compose.yaml.j2
- Test scripts: https://gitlab.com/brutus333/registrations/-/tree/master/backend/tests; results might be checked in gitlab UI if I receive project membership requests
- Ansible playbook: https://gitlab.com/brutus333/registrations/-/tree/master/deployment/ansible/deploy.yml, inventory: https://gitlab.com/brutus333/registrations/-/tree/master/deployment/ansible/hosts.yml
- VM Link: http://35.205.115.181/


