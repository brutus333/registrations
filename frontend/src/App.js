import React, { Component } from 'react';
import './App.css';
import Results from './components/Results';
import NewRegisteredUser from './components/NewRegistration';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: '',
      age: '',
      phoneNumber: '',
      registeredUsers: [],
      loading: true
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
      const value = event.target.value;
      this.setState({
        ...this.state,
        [event.target.name]: value
      });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ full_name: this.state.fullName, age: this.state.age, phone_number: this.state.phoneNumber })
    };
    this.setState({
      ...this.state,
      loading: true,
    })
    await fetch('/registrations', requestOptions).then((response) => {
       if(!response.ok) {
          alert('Failed update: ' + response.statusText)
       }
    });
    this.getNames()
  }

  getNames() {
    fetch('/registrations')
      .then(response => response.json())
      .then(json => {
        this.setState({
          fullName: '',
          age: '',
          phoneNumber: '',
          registeredUsers: json,
          loading: false
        })
      })
  }

  componentDidMount() {
    this.getNames();
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <NewRegisteredUser handleChange={this.handleChange} handleSubmit={this.handleSubmit} fullName={this.state.fullName} age={this.state.age} phoneNumber={this.state.phoneNumber} />
          {this.state.loading ? <h1>Loading</h1> : <Results {...this.state} />}
        </header>
      </div>
    );
  }
}

export default App;
