import React, { Component } from 'react';

class NewRegisteredUser extends Component {
    render() {
        return (
            <div>
                <form onSubmit={this.props.handleSubmit}>
                    <input
                        type="text"
                        name="fullName"
                        value={this.props.fullName}
                        onChange={this.props.handleChange}
                        placeholder="Full Name"
                        autoFocus
                        autoComplete='off'
                    />
                    <input
                        type="text"
                        name="age"
                        value={this.props.age}
                        onChange={this.props.handleChange}
                        placeholder="Age"
                        autoFocus
                        autoComplete='off'
                    />
                    <input
                        type="text"
                        name="phoneNumber"
                        value={this.props.phoneNumber}
                        onChange={this.props.handleChange}
                        placeholder="Phone Number"
                        autoFocus
                        autoComplete='off'
                    />
                    <button type="submit">Add</button>
                </form>
            </div>
        )
    }
}

export default NewRegisteredUser;