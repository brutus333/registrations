import React, { Component } from 'react';

class TableRow extends Component {
    render() {
        var row = this.props.row;
        return (
            <tr>
              <td>{row.full_name}</td>
              <td>{row.age}</td>
              <td>{row.phone_number}</td>
            </tr>
        )
    }
}

class Results extends Component {
    render() {
        var heading = ['Full Name','Age','Phone number']
        return (
            <div>
                <table style={{ width: 500 }}>
                    <thead>
                        <tr>
                            {heading.map(head => <th>{head}</th>)}
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.registeredUsers.map(row => <TableRow row={row} />)}
                    </tbody>
                 </table>
             </div>
            )
    }
}

export default Results;
